#include <Servo.h> // se incluye la libreria para controlar los servos

Servo servo;  // crear un objeto para controlar el servo

void setup() {
    servo.attach(9);  // el pin nueve estará conectado al cable de datos del servo.
}

void loop() {
    servo.write(0); // se mueve el servo a la posición inicial 0°
    delay(500); // se espera medio segundo para que llegue hasta la posición
    servo.write(180); // se mueve el servo a la posición máxima 180°
    delay(500); // se espera medio segundo para que llegue hasta la posición
}
