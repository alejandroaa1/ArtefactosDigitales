// Porgrama para controlar el servo 28BYJ a través del driver

#define IN1  8 // pines para controlar las bobinas del servomotor
#define IN2  9
#define IN3  10
#define IN4  11
#define STEP_COUNT 8

// un array para representar el estado de las bobina, 1 es HIGH, 0 es LOW

byte stepArray [ 8 ][ 4 ] =
  {   {1, 0, 0, 0},
      {1, 1, 0, 0},
      {0, 1, 0, 0},
      {0, 1, 1, 0},
      {0, 0, 1, 0},
      {0, 0, 1, 1},
      {0, 0, 0, 1},
      {1, 0, 0, 1}
   };

int currentStep = 0; // lleva la cuenta de cual es el paso actual
int delayBetweenStep = 2; // demora entre la actualización del paso

void setup(){
  pinMode(IN1, OUTPUT); // cada uno de los pin se usara de salida
  pinMode(IN2, OUTPUT); 
  pinMode(IN3, OUTPUT); 
  pinMode(IN4, OUTPUT); 
}

void loop() {
  doAStep(); // funcion que hace que el servo de un paso
  
}

void doAStep()
{
  digitalWrite( IN1, stepArray[currentStep][ 0] ); // se copia el estado del array a cada pin del controlador
  digitalWrite( IN2, stepArray[currentStep][ 1] );
  digitalWrite( IN3, stepArray[currentStep][ 2] );
  digitalWrite( IN4, stepArray[currentStep][ 3] );
  currentStep+=1; // se incrementa el indice del paso actual
  if (currentStep>=STEP_COUNT){ // el numero de step siempre oscilara entre 0 y STEP_COUNT
    currentStep = 0;
  }
  delay(delayBetweenStep); // demora entre pasos
}
